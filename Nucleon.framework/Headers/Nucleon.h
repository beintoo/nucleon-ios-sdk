// Copyright 2020 Beintoo Inc.

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT double NucleonVersionNumber;

FOUNDATION_EXPORT const unsigned char NucleonVersionString[];
